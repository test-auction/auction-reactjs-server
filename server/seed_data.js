const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./server/db/data.db', (err) => {
	if (err) {
		console.error(err.message);
	}
});

let sql = `SELECT * FROM auction`;

db.all(sql, [], (err, rows) => {
	if (err) {
		throw err;
	}
	rows.forEach((row) => {
		console.log(row.id);
	});
});

db.close();