const express = require('express');
const app = express();
const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./server/db/data.db', (err) => {
	if (err) {
		console.error(err.message);
	}
});

app.get('/', function (req, res) {
	res.send('Hello World!');
});

app.listen(3333, function () {
	console.log('3333');
});