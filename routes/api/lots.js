const faker = require('faker');

const getLots = (req, res, next) => {

    const N = 100;
    let lots = [];

    for (let i = 0; i < N; i++) {
        lots.push(
            {
                id: i,
                title: faker.name.findName(),
                description: faker.lorem.sentence(),
                price: faker.finance.amount(),
                step_price: 1
            }
        );
    }

    res.send(lots);

};

module.exports = getLots;
