const express = require('express');
const router = express.Router();

const lots = require('./lots');

router.use('/lots', lots);

module.exports = router;